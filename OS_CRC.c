/**
* @file    Crc.c
* @brief   Used for calculating a 32bit-CRC with a look-up-table
* @author  Kraemer Eduard
* @date    06.12.2019
* @version $Rev: 1151 $
*/

#include "OS_CRC.h"

#if USE_OS_CRC
    
/********************************* includes **********************************/

/***************************** defines / macros ******************************/
#define INVERT_CRC  (0)
    
/************************ local data type definitions ************************/
static u32 aulLut[] =
{
    0x00000000,0x1DB71064,0x3B6E20C8,0x26D930AC,0x76DC4190,0x6B6B51F4,0x4DB26158,0x5005713C,
    0xEDB88320,0xF00F9344,0xD6D6A3E8,0xCB61B38C,0x9B64C2B0,0x86D3D2D4,0xA00AE278,0xBDBDF21C
};

/************************* local function prototypes *************************/

/************************* local data (const and var) ************************/

/************************ export data (const and var) ************************/

/****************************** local functions ******************************/

/****************************** extern functions ******************************/
//********************************************************************************
/*!
\author  Kraemer E
\date    07.04.2021
\brief   Crc32 calculation. See https://create.stephan-brumme.com/crc32/#half-byte
\param   pucData       - Pointer onto byte array to calculate CRC32
\param   ulLength      - Size of buffer to calculate CRC
\param   ulPreviousCrc - Prevoius/start value for CRC calculation
\return  Crc32 value for given buffer
***********************************************************************************/
u32 OS_Crc32Buffer(u8* pucData, u32 ulLength, u32 ulPreviousCrc)
{
    #if INVERT_CRC
        u32 ulCrc = ~ulPreviousCrc;
    #else
        u32 ulCrc = ulPreviousCrc;
    #endif

    u8* pucCurrent = pucData;

    while (ulLength--)
    {
        ulCrc = aulLut[(ulCrc ^  *pucCurrent      ) & 0x0F] ^ (ulCrc >> 4);
        ulCrc = aulLut[(ulCrc ^ (*pucCurrent >> 4)) & 0x0F] ^ (ulCrc >> 4);
        pucCurrent++;
    }

    #if INVERT_CRC
        return ~ulCrc;
    #else
        return ulCrc;
    #endif
}


//********************************************************************************
/*!
\author  Kraemer E
\date    07.04.2021
\brief   Crc32 for single byte
\param   ucData        - Data byte to calculate CRC32
\param   ulPreviousCrc - Prevoius/start value for CRC calculation
\return  Crc32 value
***********************************************************************************/
u32 OS_Crc32Byte(u8 ucData, u32 ulPreviousCrc)
{
    #if INVERT_CRC
        u32 ulCrc = ~ulPreviousCrc;
    #else
        u32 ulCrc = ulPreviousCrc;
    #endif

    ulCrc = aulLut[(ulCrc ^  ucData      ) & 0x0F] ^ (ulCrc >> 4);
    ulCrc = aulLut[(ulCrc ^ (ucData >> 4)) & 0x0F] ^ (ulCrc >> 4);

    #if INVERT_CRC
        return ~ulCrc;
    #else
        return ulCrc;
    #endif
}


//********************************************************************************
/*!
\author  Kraemer E
\date    07.04.2021
\brief   Checks if buffer valid with assumption that last 4 bytes in given buffer is CRC32
         calculated using Crc32Buffer() with CRC_INITIAL_VALUE
\param   pucData    - Pointer onto buffer to check
\param   ulLength   - Size of buffer including CRC at the end
\return  true when buffer contains valid CRC32 at the end
***********************************************************************************/
bool OS_Crc32IsBufferValid(u8* pucData, u32 ulLength)
{
    bool bRes = false;

    // At least needs one data byte to calculate CRC
    if (ulLength > sizeof(u32))
    {
        const u32 ulCalcCrc = OS_Crc32Buffer(pucData, ulLength - sizeof(u32), CRC_INITIAL_VALUE);
        // This cause hardfault exception if trying to cast odd address (when ulLength%2 != 0)
//        const u32 ulIsCrc   = *((u32*)&pucData[ulLength - sizeof(u32)]);
        u32 ulIsCrc = 0;
        ulIsCrc += pucData[ulLength - sizeof(u32) + 3];
        ulIsCrc <<= 8;
        ulIsCrc += pucData[ulLength - sizeof(u32) + 2];
        ulIsCrc <<= 8;
        ulIsCrc += pucData[ulLength - sizeof(u32) + 1];
        ulIsCrc <<= 8;
        ulIsCrc += pucData[ulLength - sizeof(u32) + 0];

        bRes = (bool)(ulCalcCrc == ulIsCrc);
    }
    return bRes;
}

#endif //USE_OS_CRC
