
#ifndef _OS_CRC_H
#define _OS_CRC_H

#include "OS_Config.h"
#if USE_OS_CRC       
    
/********************************* includes **********************************/
#include "BaseTypes.h"
/***************************** defines / macros ******************************/

/****************************** type definitions *****************************/

/***************************** global variables ******************************/

/************************ externally visible functions ***********************/
#ifdef __cplusplus
extern "C"
{
#endif

u32  OS_Crc32Buffer(u8* pucData, u32 ulLength, u32 ulPreviousCrc);
u32  OS_Crc32Byte(u8 ucData, u32 ulPreviousCrc);
bool OS_Crc32IsBufferValid(u8* pucData, u32 ulLength);

#ifdef __cplusplus
}
#endif

#endif //USE_OS_CRC

#endif // _OS_CRC_H

